# Changelog


## v0.1.1 (2022-06-10)

### Features

* Add customizable categories. [Stavros Korokithakis]

* Add stochasticity argument. [Stavros Korokithakis]

* Add pre-commit hook. [Stavros Korokithakis]

### Fixes

* Change author name display. [Stavros Korokithakis]

* Fix cli interface. [Stavros Korokithakis]

* Actually fix types. [Stavros Korokithakis]

* Fix types. [Stavros Korokithakis]

* Fix bug where the first month was skipped. [Stavros Korokithakis]

* Make project name optional. [Stavros Korokithakis]


