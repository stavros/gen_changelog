gen_changelog
=============

gen_changelog is a very simple script that generates a changelog from a repository with
commit messages compatible with [Conventional
Commits](https://www.conventionalcommits.org/). The main difference of this script from
[gitchangelog](https://github.com/sarnold/gitchangelog) is that this script will
generate sections from dates/months instead of git tags of versions.


Installation
------------

Installing gen_changelog is simple. You can use `pipx` (recommended):

```bash
$ pipx install gen-changelog
```

Or `pip` (less recommended):

```bash
$ pip install gen-changelog
```


Usage
-----

Just run it in the root of your repository:

```bash
$ ./gen_changelog "Your project's name"
```

You're done.


Customization
-------------

You can customize the sections you want in the changelog by passing the `--categories`
parameter:

```bash
$ ./gen_changelog --categories "add:Added|rem:Removed|chg:Changed|cs:CS:GO stuff"
```


With pre-commit
---------------

To use this with pre-commit, add this to your `.pre-commit-config.yaml`:

```yaml
- repo: https://gitlab.com/stavros/gen_changelog.git
  rev: ccc6fa1dbe1a937c9b729e356b5fd91bf9d59ca4
  hooks:
  - id: gen-changelog
    stages: [commit]
```
